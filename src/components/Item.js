// React libraries
import React, {Component} from 'react';
import {Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Text, Button, Icon} from 'react-native-elements';
import Colors from "../constants/Colors";
import Languague from '../constants/Language'
import Config from "../constants/Config";
import realm from '../model/realm'

export default class Item extends Component {

    state = {
        item: null
    }

    componentDidMount() {
        this.setState({
            item: this.props.item
        })
    }

    render() {
        if (this.state.item == null) {
            return (<View/>)
        } else {
            return (
                <View>
                    <View style={styles.container}>
                        <View style={styles.row}>
                            <Image style={styles.image}
                                   source={{
                                       uri: Config.endpoints.host + Config.endpoints.mediaPath +
                                       (this.state.item.miniature == null ? 'safy.png' : this.state.item.miniature)
                                   }}/>
                            <View
                                style={styles.info}>
                                <Text
                                    style={styles.itemCode}>
                                    {this.state.item.name}
                                </Text>
                                <Text>{Languague.cart.price}
                                    <Text
                                        style={{color: 'red'}}>
                                        {' ' + Languague.currency + ' ' + this.state.item.price}
                                    </Text>
                                </Text>
                                <View
                                    style={{
                                        alignSelf: 'flex-end',
                                        paddingRight: 10,
                                        flexDirection: 'row'
                                    }}>
                                    <TouchableOpacity style={[styles.roundedButton, styles.minusButton]}
                                                      onPress={this.remove.bind(this)}
                                    >
                                        <Text
                                            style={
                                                {
                                                    fontSize: 24,
                                                    fontWeight: 'bold',
                                                    color: '#fff'
                                                }
                                            }
                                        >-</Text>
                                    </TouchableOpacity>
                                    <Text
                                        style={{
                                            fontSize: 32,
                                            textAlign: 'center',
                                            marginLeft: 5,
                                            marginRight: 5
                                        }}
                                    >
                                        {this.state.item.quantity}
                                    </Text>
                                    <TouchableOpacity style={[styles.roundedButton, styles.plusButton]}
                                                      onPress={this.add.bind(this)}
                                    >
                                        <Text
                                            style={
                                                {
                                                    fontSize: 24,
                                                    fontWeight: 'bold',
                                                    color: '#fff'
                                                }
                                            }
                                        >+</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                    </View>

                </View>
            );
        }
    }

    /**
     * Add To Cart
     */
    add() {
        realm.write(() => {
            this.props.item.quantity = this.props.item.quantity + 1
        })


        this.setState({
            item: this.props.item
        })
    }


    /**
     * Remove Element Of Cart
     */
    remove() {
        if (this.props.item.quantity === 1) {

            realm.write(() => {
                realm.delete(this.props.item)
            })

            this.props.context.getProducts()

        } else {
            realm.write(() => {
                this.props.item.quantity = this.props.item.quantity - 1
            })


            this.setState({
                item: this.props.item
            })
        }
    }


}

const styles = StyleSheet.create({
        container: {

            backgroundColor: '#fff',
            padding: 5,
            margin: 10,
            marginBottom: 5,
            borderRadius: 10,
            elevation: 5
        },
        row: {
            flexDirection: 'row'
        },
        itemCode: {
            fontSize: 24,
            fontWeight: '400',
        },
        image: {
            width: 100,
            height: 100,
            alignSelf: 'center'
        },
        roundedButton: {
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            height: 40,
            borderRadius: 100,
        },
        plusButton: {
            backgroundColor: Colors.successColor,
        },
        minusButton: {
            backgroundColor: Colors.dangerColor,
        },
        info: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            marginLeft: 5
        },
        controls: {
            flex: 1,
            flexDirection: 'column'
        }
    })
;
