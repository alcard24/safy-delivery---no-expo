// React libraries
import React, {Component} from 'react';
import {Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Text, Button, Icon} from 'react-native-elements';
import Colors from "../constants/Colors";
import Languague from '../constants/Language'

export default class Order extends Component {

    render() {
        return (
            <View>
                <View style={styles.container}>
                    <View style={styles.row}>
                        <Image style={styles.image}
                               source={require('../assets/images/iconorder.png')}/>
                        <View
                            style={styles.info}>
                            <Text
                                style={styles.orderCode}>
                                {this.props.item.codigo}
                            </Text>
                            {/*<Text*/}
                                {/*style={styles.textHeaderDetail}>*/}
                                {/*{Languague.order.date + ' '}*/}
                                {/*<Text*/}
                                    {/*style={styles.textDetail}>*/}
                                    {/*{this.props.date}*/}
                                {/*</Text>*/}
                            {/*</Text>*/}

                            <Text
                                style={styles.textHeaderDetail}>
                                {Languague.order.total + ' '}
                                <Text
                                    style={styles.textDetail}>
                                    {this.props.item.monto}
                                </Text>
                            </Text>


                            <Text
                                style={styles.textHeaderDetail}>
                                {Languague.order.state + ' '}
                                <Text
                                    style={styles.textDetail}>
                                    {this.props.item.estado}
                                </Text>
                            </Text>
                            <View
                                style={{
                                    alignSelf: 'flex-end',
                                    paddingRight: 10,
                                    flexDirection: 'row'
                                }}>
                                <Button
                                    title={Languague.order.cancel}
                                    buttonStyle={{
                                        backgroundColor: Colors.defaultColor,
                                        width: 75,
                                        height: 45,
                                        marginTop: 5,
                                        borderColor: "transparent",
                                        borderWidth: 0,
                                        borderRadius: 5
                                    }}
                                    onPress={this.cancelOrder.bind(this)}
                                />
                            </View>
                        </View>
                    </View>

                </View>

            </View>
        );
    }

    cancelOrder(){
        this.props.context.cancelOrderRequest(this.props.item);
    }


}

const styles = StyleSheet.create({
        container: {
            backgroundColor: '#fff',
            padding: 5,
            paddingBottom: 10,
            margin: 10,
            marginBottom: 5,
            borderRadius: 10,
            elevation: 5
        },
        orderCode: {
            fontSize: 18,
            fontWeight: '400',
            marginBottom: 5
        },
        textHeaderDetail: {
            fontSize: 16,
            fontWeight: 'bold'
        },
        textDetail: {
            fontSize: 16,
            textAlign: 'right',
            alignSelf: 'stretch',
            fontWeight: 'normal'
        },
        row: {
            flexDirection: 'row'
        },
        image: {
            width: 75,
            height: 75,
            alignSelf: 'center'
        },
        roundedButton: {
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            height: 40,
            borderRadius: 100,
        },
        plusButton: {
            backgroundColor: Colors.successColor,
        },
        minusButton: {
            backgroundColor: Colors.dangerColor,
        },
        info: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            marginLeft: 5
        },
        controls: {
            flex: 1,
            flexDirection: 'column'
        }
    })
;
