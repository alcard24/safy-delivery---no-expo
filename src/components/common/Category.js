//
// This is the Category class
//

// React native and others libraries imports
import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-elements';
import {Card} from 'react-native-elements'

import Config from "../../constants/Config";

// Our custom files and classes import

export default class Category extends Component {

    render() {
        return (
            <TouchableOpacity
                onPress={this.onCategoryClicked.bind(this)}
            >
                <View>
                    <Card
                        title={this.props.item.descripcion}
                        image={{uri: Config.endpoints.host + Config.endpoints.mediaPath + this.props.item.pathImage}}>
                    </Card>
                </View>
            </TouchableOpacity>
        );
    }

    onCategoryClicked() {
        if (this.props.item.terminal !== true){
            this.props.nav.push("CategoriesScreen", {
                title: this.props.item.descripcion,
                idCategory: this.props.item.id
            });
        } else {
            this.props.nav.push("Products", {
                title: this.props.item.descripcion,
                category: this.props.item.id
            });
        }
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 250
    },
    image: {
        flex: 1
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    info: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nom: {
        fontSize: 24,
        fontWeight: '400',
        color: '#fff',
        textAlign: 'center'
    }
});
