// React Libraries
import React, {Component} from 'react'

// Custom Libraries
import Item from '../Item'
import realm from '../../model/realm'
import Config from "../../constants/Config";
import Language from "../../constants/Language";
import Colors from "../../constants/Colors";
import {StyleSheet, View, ScrollView, RefreshControl} from "react-native";
import AwesomeAlert from 'react-native-awesome-alerts';

export default class Items extends Component {

    state = {
        items: [],
        refreshing: false,
    }

    componentDidMount() {
        this.getProducts();
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh.bind(this)}
                        />
                    }>
                    {
                        this.state.items.map((item, index) => {
                            return (
                                <Item
                                    key={index}
                                    item={item}
                                    context={this}
                                />
                            )
                        })
                    }
                </ScrollView>
                <AwesomeAlert
                    showProgress={true}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={false}
                    show={(this.state.refreshing && Config.showSweetRefreshing) || this.state.anotherLoadProcess}
                />
            </View>
        );
    }

    _onRefresh() {
        this.getProducts()
    }
    getProducts() {
        this.setState({
            items: realm.objects('OrderDetail')
        });
    }
}


const styles = StyleSheet.create({
        container: {
            width: '100%',
            height: '100%'
        },
        button: {
            margin: 10,
            paddingHorizontal: 10,
            paddingVertical: 7,
            borderRadius: 5,
            backgroundColor: "#AEDEF4",
        },
        text: {
            color: '#fff',
            fontSize: 15
        }
    }
);