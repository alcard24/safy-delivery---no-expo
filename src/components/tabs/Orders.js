// React Libraries
import React, {Component} from 'react'
import {View, ScrollView, RefreshControl, StyleSheet} from 'react-native'
// Custom Libraries
import Order from '../Order'
import Config from "../../constants/Config";
import axios from 'axios'
import AwesomeAlert from 'react-native-awesome-alerts';
import Colors from "../../constants/Colors";
import Language from "../../constants/Language";

export default class Orders extends Component {

    state = {
        showAlert: false,
        items: [],
        selectedOrder: null,
        refreshing: false,
        anotherLoadProcess: false,
        message: null,
        showAlertMessage: false
    }

    componentDidMount() {
        this.getOrders()
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>
                    {
                        this.state.items.map((item, index) => {
                            return (
                                <Order
                                    key={index}
                                    item={item}
                                    context={this}
                                />
                            )
                        })
                    }
                </ScrollView>
                <AwesomeAlert
                    showProgress={true}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={false}
                    show={(this.state.refreshing && Config.showSweetRefreshing) || this.state.anotherLoadProcess}
                />
                <AwesomeAlert
                    title={this.state.message}
                    show={this.state.showAlertMessage}
                />
                <AwesomeAlert
                    show={this.state.showAlert}
                    title={Language.order.cancelOrder}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText={Language.negativeButton}
                    confirmText={Language.positiveButton}
                    cancelButtonColor={Colors.dangerColor}
                    confirmButtonColor={Colors.successColor}
                    titleStyle={{fontSize: 22}}
                    confirmButtonStyle={{minWidth: 75}}
                    cancelButtonStyle={{minWidth: 75}}
                    confirmButtonTextStyle={{fontSize: 18, textAlign: 'center'}}
                    cancelButtonTextStyle={{fontSize: 18, textAlign: 'center'}}
                    onCancelPressed={() => {
                        this.closeAlert()
                    }}
                    onConfirmPressed={() => {
                        this.confirmCancel()
                    }}
                />

            </View>
        );
    }

    getOrders() {
        var url = Config.endpoints.host + Config.endpoints.getOrders + Config.entityCode;

        axios(
            {
                method: 'get',
                url: url,
                responseType: 'json'
            })
            .then(response => {
                const orders = response.data;
                this.setState({
                    items: orders,
                    refreshing: false
                });
            })
            .catch(function (error) {
                console.log(error);
            })

    }

    cancelOrderRequest(order) {
        if (order !== null) {
            this.setState({
                showAlert: true,
                selectedOrder: order
            })
        }
    }

    confirmCancel() {
        var url = Config.endpoints.host + Config.endpoints.cancelOrder;

        var context = this;

        context.setState({
            showAlert: false,
            anotherLoadProcess: true
        })

        console.log(url)
        console.log(context.state.selectedOrder.id)
        axios(
            {
                method: 'post',
                url: url,
                responseType: 'json',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            },
            {
                idpedido: context.state.selectedOrder.id
            }
        ).then(response => {
            context.setState({
                message: response.data.mensaje,
                showAlertMessage: false,
                anotherLoadProcess: true
            })

            this.getOrders()
        }).catch(function (error) {
            context.setState({
                showAlertMessage: false,
                anotherLoadProcess: true
            })
            console.log(error);
        })
    }

    closeAlert() {
        this.setState({
            showAlert: false,
            selectedOrder: null
        })
    }

    closeAlertMessage() {
        this.setState({
            showAlertMeessage: false,
            showAlertMessage: null,
        })
    }
}

const styles = StyleSheet.create({
        container: {
            width: '100%',
            height: '100%'
        },
        button: {
            margin: 10,
            paddingHorizontal: 10,
            paddingVertical: 7,
            borderRadius: 5,
            backgroundColor: "#AEDEF4",
        },
        text: {
            color: '#fff',
            fontSize: 15
        }
    }
);
