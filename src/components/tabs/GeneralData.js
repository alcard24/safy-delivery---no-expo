import React, {Component} from 'react'
import {View} from 'react-native'
import {FormLabel, FormInput} from 'react-native-elements'
import Language from '../../constants/Language'
import realm from '../../model/realm'
import Colors from "../../constants/Colors";


export default class GeneralData extends Component {

    state = {
        name: '',
        phone: '',
        email: '',
        adress: '',
        lastedClient: null,
    }

    componentDidMount() {
        var clients = realm.objects('Client');

        if (clients.length  > 0) {
            // Always the same
            var lastedClient = clients[clients.length  - 1];

            this.setState({
                name: lastedClient.name,
                phone: lastedClient.phone,
                email: lastedClient.email,
                adress: lastedClient.adress,
                lastedClient: lastedClient
            })
        }
    }

    render() {
        return (
            <View>
                <FormLabel>{Language.billing.name}</FormLabel>
                <FormInput
                    underlineColorAndroid={Colors.colorAccent}
                    value={this.state.name}
                    onChangeText={(text) => this.setState({name: text})}
                />
                <FormLabel>{Language.billing.phone}</FormLabel>
                <FormInput
                    underlineColorAndroid={Colors.colorAccent}
                    value={this.state.phone}
                    onChangeText={(text) => this.setState({phone: text})}
                />
                <FormLabel>{Language.billing.email}</FormLabel>
                <FormInput
                    underlineColorAndroid={Colors.colorAccent}
                    value={this.state.email}
                    onChangeText={(text) => this.setState({email: text})}
                />
                <FormLabel>{Language.billing.adress}</FormLabel>
                <FormInput
                    underlineColorAndroid={Colors.colorAccent}
                    multiline
                    lines={4}
                    value={this.state.adress}
                    onChangeText={(text) => this.setState({adress: text})}
                />

            </View>
        )
    }

    saveLastedClient() {
        if (this.state.phone !== '' && this.state.email !== '' &&
            this.state.adress !== '' && this.state.name !== '') {
            if (this.state.lastedClient != null) {
                realm.write(() => {
                    this.state.lastedClient.name = this.state.name;
                    this.state.lastedClient.phone = this.state.phone;
                    this.state.lastedClient.email = this.state.email;
                    this.state.lastedClient.adress = this.state.adress;
                })
            } else {

                realm.write(() => {
                    realm.create('Client', {
                        name: this.state.name,
                        email: this.state.email,
                        phone: this.state.phone,
                        adress: this.state.adress
                    });
                })
            }

            this.props.context.navigateToSecondPage()

        } else {
            this.props.context.showMessage('Verifique los datos')
        }
    }
}