//
// This is the Commandedetails class
//

// React native and others libraries imports
import React, {Component} from 'react';
import {ScrollView, View, StyleSheet, TouchableWithoutFeedback, Alert, WebView, ActivityIndicator} from 'react-native';
import {Text, Divider, Button, Icon} from 'react-native-elements';
import {Select, Option} from "react-native-chooser";
import {NavigationActions} from 'react-navigation';

// Our custom files and classes import
import Config from '../../constants/Config';
import Language from "../../constants/Language";
import realm from '../../model/realm'

export default class Sumary extends Component {
    state = {
        items: [],
        total: 0.0,
        quantity: 0,
        tva: 0.0,
    };


    componentDidMount() {
        var items = realm.objects('OrderDetail');
        var total = 0
        var quantity = 0

        items.forEach(listItem => {
            total += listItem.price
            quantity += listItem.quantity
        });
        this.setState({
            items: items,
            total: total,
            quantity: quantity
        })
    }

    render() {

        return (
            <View style={{backgroundColor: '#fdfdfd', flex: 1}}>
                {this.renderMain()}
            </View>
        );
    }

    renderMain() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.content}>
                    {
                        this.state.items.map((item, i) => {
                            return (
                                <View style={styles.row} key={i}>
                                    <Text style={styles.firstColumn}>{item.quantity + ' - ' + item.name}</Text>
                                    <Text style={[styles.column, styles.red]}>{Language.currency + ' ' + item.price}</Text>
                                </View>
                            )
                        })

                    }
                    <Divider style={styles.divider}/>
                    <View style={styles.row}>
                        <Text
                            style={[styles.firstColumn, styles.header, styles.total]}>{Language.summary.quantity}</Text>
                        <Text
                            style={[styles.firstColumn, styles.red, styles.total, styles.textRight]}>{this.state.quantity}</Text>
                    </View>
                    <View>
                        <Text
                            style={[styles.firstColumn, styles.header, styles.total]}>{Language.summary.total}</Text>
                        <Text
                            style={[styles.firstColumn, styles.red, styles.total, styles.textRight]}>{this.state.total}</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        paddingLeft: 12,
        paddingRight: 12,
        backgroundColor: '#fdfdfd'
    },
    content: {
        paddingTop: 20,
        paddingBottom: 10
    },
    row: {
        flexDirection: 'row',
        marginBottom: 12
    },
    firstColumn: {
        flex: 3,
        fontSize: 18
    },
    header: {
        fontWeight: 'bold'
    },
    column: {
        flex: 1,
        fontSize: 18
    },
    text: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    red: {
        color: "#e74c3c"
    },
    blue: {
        color: "#34495e"
    },
    total: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    divider: {
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: "#cccccc"
    },
    option: {
        flex: 1,
        flexDirection: 'row',
    },
    optionText: {
        flex: 2,
        marginLeft: 10,
        fontSize: 18
    },
    textRight: {
        textAlign: 'right'
    },
});
