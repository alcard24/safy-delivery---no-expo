// React libraries
import React, {Component} from 'react';
import {Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Text, Button, Icon} from 'react-native-elements';
import Colors from "../constants/Colors";
import Languague from '../constants/Language'
import Config from "../constants/Config";
import realm from '../model/realm'

export default class Product extends Component {

    render() {
        return (
            <View>
                <View style={styles.container}>
                    <View style={styles.row}>
                        <Image style={styles.image}
                               source={{uri: Config.endpoints.host + Config.endpoints.mediaPath + this.props.item.pathImage}}/>
                        <View
                            style={styles.info}>
                            <Text
                                style={styles.productName}>
                                {this.props.item.nombre}
                            </Text>
                            <Text
                                style={styles.textHeaderDetail}>
                                {Languague.cart.price + ' '}
                                <Text
                                    style={styles.textDetail}>
                                    {this.props.item.precioVenta}
                                </Text>
                            </Text>
                            <View
                                style={{
                                    alignSelf: 'flex-end',
                                    paddingRight: 10,
                                    flexDirection: 'row'
                                }}>
                                <Button
                                    title={Languague.product.add}
                                    buttonStyle={{
                                        backgroundColor: Colors.defaultColor,
                                        width: 75,
                                        height: 45,
                                        marginTop: 5,
                                        borderColor: "transparent",
                                        borderWidth: 0,
                                        borderRadius: 5
                                    }}
                                    onPress={this.add.bind(this)}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    /**
     * Add To Cart
     */
    add() {

        // Check Product is in existence

        var products = realm.objects('OrderDetail').filtered(
            'id = "' + this.props.item.id + '"'
        )

        if (products.length > 0) {
            var product = products[0];

            realm.write(() => {
                product.quantity = product.quantity + 1
            })
        } else {
            var item = this.props.item;


            realm.write(() => {
                realm.create('OrderDetail', {
                    id: item.id.toString(),
                    name: item.nombre,
                    price: item.precioVenta,
                    quantity: 1,
                    attributeId: 0,
                    description: '',
                    priceAttribute: '0',
                    allowDelivery: true,
                    miniature: (item.pathImage === null ? 'safy.png' : item.pathImage)
                })
            })
        }

        this.props.context.showToast(this.props.item.nombre)

    }


}

const styles = StyleSheet.create({
        container: {
            backgroundColor: '#fff',
            padding: 5,
            paddingBottom: 10,
            margin: 10,
            marginBottom: 5,
            borderRadius: 10,
            elevation: 5
        },
        productName: {
            fontSize: 18,
            fontWeight: '400',
            marginBottom: 5
        },
        textHeaderDetail: {
            fontSize: 16,
            fontWeight: 'bold'
        },
        textDetail: {
            fontSize: 16,
            textAlign: 'right',
            alignSelf: 'stretch',
            fontWeight: 'normal',
            color: Colors.dangerColor
        },
        row: {
            flexDirection: 'row'
        },
        image: {
            width: 75,
            height: 75,
            alignSelf: 'center'
        },
        roundedButton: {
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            height: 40,
            borderRadius: 100,
        },
        plusButton: {
            backgroundColor: Colors.successColor,
        },
        minusButton: {
            backgroundColor: Colors.dangerColor,
        },
        info: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            marginLeft: 5
        },
        controls: {
            flex: 1,
            flexDirection: 'column'
        }
    })
;
