//
// This is the Categories class
//

// React native and others libraries imports
import React, {Component} from 'react';
import { StyleSheet, View} from 'react-native';
import axios from 'axios'
import GridList from 'react-native-grid-list'
import {Card} from 'react-native-elements'
// Our custom files and classes import
import Config from "../constants/Config";

export default class Categories extends Component {

    state = {
        categories: []
    }

    componentDidMount() {
        this.getCategories();
    }

    renderItem = ({item, index}) => (
        <View>
            <Card
                title={item.descripcion}
                image={{uri: Config.endpoints.host + Config.endpoints.mediaPath + item.pathImage}}>
            </Card>
        </View>
    );

    render() {
        return (
            <View>
                <GridList
                    data={this.state.categories}
                    numColumns={2}
                    renderItem={this.renderItem}
                />
            </View>
        );
    }

    seeMenu(consommable) {
        // this.props.navigation.navigate("Consommables", {title: this.props.navigation.state.params.resto.nom, resto: this.props.navigation.state.params.resto, consommables: consommable});
    }

    getCategories() {
        var url = Config.endpoints.host + Config.endpoints.category + Config.entityCode;

        axios(
            {
                method: 'get',
                url: url,
                responseType: 'json'
            })
            .then(response => {
                const categories = response.data;
                this.setState({
                    categories: categories
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}

const styles = StyleSheet.create({

});
