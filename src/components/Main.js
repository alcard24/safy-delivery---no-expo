// React Native Libraries
import React, {Component} from 'react';
import {View, TouchableHighlight, StyleSheet} from 'react-native';
import {Icon, Badge} from 'react-native-elements';
import {createStackNavigator, NavigationActions} from 'react-navigation';
// Custom Imports

import Languague from '../constants/Language'
import Colors from '../constants/Colors'
// Screen Imports
import LoginScreen from '../screens/LoginScreen'
import CategoryScreen from '../screens/CategoryScreen'
import Categories from "./tabs/Categories";
import TabNavigation from '../navigation/TabNavigation'
import BillingScreen from '../screens/BillingScreen'
import ProductScreen from '../screens/ProductScreen'
// Ignore the "Setting a timer for a long time" warning
console.ignoredYellowBox = ['Setting a timer'];

const styles = StyleSheet.create({
    header: {
        backgroundColor: Colors.primaryColor
    },
    title: {
        color: Colors.titleColor
    },
    rightButtonContainer: {
        paddingRight: 20,
        paddingLeft: 20,
        flex: 1,
        justifyContent: 'center'
    }
});

const right = (navigation) => (
    <View style={{
        flex: 1,
        flexDirection: 'row'
    }}>
        <TouchableHighlight style={styles.rightButtonContainer} underlayColor="rgba(180,59,47,0.7)"
                            onPress={() => btnClicked(navigation)}>
            <View>
                <Icon
                    name='shopping-cart'
                    type='FontAwesome5'
                    color='#fff'
                />
            </View>

        </TouchableHighlight>

    </View>
);

const reload = (state) => (
    <TouchableHighlight style={styles.rightButtonContainer} underlayColor="rgba(180,59,47,0.7)"
                        onPress={() => state.params.reload()}>
        <View>
            <Icon
                name='sync'
                type='FontAwesome5'
                color='#fff'
            />
        </View>
    </TouchableHighlight>
);

var defaultOptions = ({navigation}) => ({
    title: Languague.title,
    headerStyle: styles.header,
    headerTitleStyle: styles.title,
    headerTintColor: "#fff",
    headerRight: navigation.state.routeName !== 'Billing' ? right(navigation) : <View/>,
});

const Router = createStackNavigator({
    Home: {screen: TabNavigation, navigationOptions: defaultOptions},
    Categories: {screen: Categories, navigationOptions: defaultOptions},
    CategoriesScreen: {screen: CategoryScreen, navigationOptions: defaultOptions},
    Billing: {screen: BillingScreen, navigationOptions: defaultOptions},
    Products: {screen: ProductScreen, navigationOptions: defaultOptions},
    Login: {screen: LoginScreen, navigationOptions: {header: null}},
}, {
    initialRouteName: 'Home'
});

function btnClicked(navigation) {
    navigation.push("Billing", {title: Languague.cart.title});
}

/**
 * Main Application, Stack Navigation or Router...
 */
export default class Main extends Component {

    render() {
        return (
            <Router/>
        )
    }
}