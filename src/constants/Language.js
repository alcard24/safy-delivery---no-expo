export default {
    title: 'Safy Delivery',
    login: {
        user: 'Usuario',
        loginBtn: "Acceder",
        signupBtn: "Registrarse",
        password: "Contraseña"
    },
    home: {},
    cart: {
        title: 'Factura',
        total: 'Total',
        price: 'Precio'
    },
    product: {
      add: 'Agregar'
    },
    order: {
        date: 'Fecha',
        total: 'Total',
        cancel: 'Anular',
        state: 'Estado',
        cancelOrder: '¿ Quieres cancelar la orden ?'

    },
    currency: 'C$',
    billing: {
        generalData: 'Datos Generales',
        summary: 'Resumen',
        name: 'Nombre',
        phone: 'Telefono',
        redeemCode: 'Redimir Cupón / Código de referido',
        insertCode: 'Ingrese código',
        reedem: 'redmir',
        email: 'Correo electronico',
        adress: 'Dirección',
        next: 'Siguiente',
        send: 'Enviar',
        previous: 'Anterior'
    },
    summary :{
        quantity: 'Cantidad',
        total: 'total'
    },
    positiveButton: 'Sí',
    negativeButton: 'No',


}