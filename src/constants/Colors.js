const tintColor = '#2f95dc';

export default {
    primaryColor: '#72390A',
    colorAccent: "#D0844D",
    titleColor: "#fff",
    successColor: '#27ae60',
    dangerColor: '#c0392b',
    defaultColor: '#3498db'
};
