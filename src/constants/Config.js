export default {
    endpoints: {
        host: "http://sistemasafy.com/safy/rest",
        mediaPath: "/productos/media_static?nombre=",
        productByEntity: "/productos/listaProductosEntidad?entidad=",
        login: '/meseros/verificacion',
        mainCategories: '/categoria/listaCategorias?entidad=',
        childCategories: '/categoria/listaCategoriasHijas?categoria_padre=',
        productsByCategory: '/productos/listaProductos?categoria=',
        getAttributes: '/atributos/lista?producto=',
        getOrders: '/pedidos/verPedidos?entidad=',
        cancelOrder: '/pedidos/anularPedido',
        sendOrder: '/pedidos/crearPedido'
    },
    vat: .15,
    paymentMethodCode: '004',
    googleMapsApiKey: "",
    entityCode: '1',
    allowCupons: true,
    showSweetRefreshing: false,
    schemaVersion: 1,
    orderType: {
        delivery: '003|01',
        restaurant: '003|02'
    }
}