// Realm Import for Use DB Operation
import Realm from 'realm'
import Config from '../constants/Config'

/**
 * Client Model
 */
class Client extends Realm.Object {
}

Client.schema = {
    name: 'Client',
    properties: {
        name: 'string',
        email: 'string',
        phone: 'string',
        adress: 'string'
    }
}

/**
 * Oder Model
 */
class Order extends Realm.Object {
}

Order.schema = {
    name: 'Order',
    properties: {
        id: 'string',
        numOrder: 'string',
        state: 'string',
        stateCode: 'string',
        date: {type: 'date', default: Date.now()},
        total: 'string',
        promoCode: 'string',
        orderType: 'string',
        table: 'string'
    }
};

/**
 * Pre Order  Model
 */
class PreOrder extends Realm.Object {
}

PreOrder.schema = {
    name: 'PreOrder',
    properties: {
        typeOfOrder: 'string',
        promoValue: 'string',
        promoCode: 'string',
        typeOfPayment: {type: 'string', default: Config.paymentMethodCode}
    }
};

/**
 * Order Detail Model
 */
class OrderDetail extends Realm.Object {
}

OrderDetail.schema = {
    name: 'OrderDetail',
    properties: {
        id: 'string',
        name: 'string',
        price: 'double',
        quantity: 'int',
        miniature: 'string',
        attributeId: 'int',
        description: 'string',
        priceAttribute: 'string',
        allowDelivery: 'bool'
    }
};


export default new Realm(
    {
        schema: [
            Client,
            Order,
            PreOrder,
            OrderDetail
        ],
        schemaVersion: Config.schemaVersion
    });
