import React, {Component} from 'react';
import {AppRegistry, StyleSheet, View, Text} from 'react-native';
import {Button, Icon} from 'react-native-elements'
import {ViewPager} from 'rn-viewpager';
import AwesomeAlert from 'react-native-awesome-alerts';

import StepIndicator from 'react-native-step-indicator';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Language from "../constants/Language";
import Colors from "../constants/Colors";

import GeneralData from '../components/tabs/GeneralData'
import Sumary from '../components/tabs/Sumary'
import axios from 'axios'
import Config from "../constants/Config";
import realm from "../model/realm";

const PAGES = ['Page 1', 'Page 2'];

const stepConfig = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#7eaec4',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#7eaec4',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#7eaec4',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#7eaec4',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#7eaec4',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#7eaec4'
}

export default class BillingScreen extends Component {

    state = {
        currentPage: 0,
        showAlertMessage: false,
        message: ''
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextState.currentPage != this.state.currentPage) {
            if (this.viewPager) {
                this.viewPager.setPage(nextState.currentPage)
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.stepIndicator}>
                    <StepIndicator stepCount={2} customStyles={stepConfig} currentPosition={this.state.currentPage}
                                   labels={[Language.billing.generalData, Language.billing.summary]}/>
                </View>
                <ViewPager
                    style={{flexGrow: 1}}
                    ref={(viewPager) => {
                        this.viewPager = viewPager
                    }}
                    onPageSelected={(page) => {
                        this.setState({currentPage: page.position})
                    }}
                    horizontalScroll={false}
                >
                    <View key={1}>
                        <GeneralData
                            ref={'general'}
                            context={this}
                        />
                    </View>

                    <View key={2}>
                        <Sumary/>
                    </View>
                </ViewPager>
                <View style={styles.navContainer}>
                    <View
                        style={{
                            alignSelf: 'flex-end',
                            flexDirection: 'row'
                        }}
                    >
                        {this.renderBack()}
                        <Button
                            iconRight={{
                                name: 'chevron-right',
                                size: 15,
                                color: Colors.defaultColor
                            }}
                            buttonStyle={{
                                backgroundColor: "transparent"
                            }}
                            color={Colors.defaultColor}
                            title={
                                this.state.currentPage === 0 ?
                                    Language.billing.next :
                                    Language.billing.send
                            }
                            onPress={this.secondButton.bind(this)}
                        />
                    </View>
                </View>
                <AwesomeAlert
                    title={this.state.message}
                    show={this.state.showAlertMessage}
                />
            </View>
        );
    }

    renderViewPagerPage = (data) => {
        return (<View key={data} style={styles.page}>
            <Text>{data}</Text>
        </View>)
    }

    renderBack = () => {
        if (this.state.currentPage === 0) {
            return null
        } else {
            return (
                <Button
                    icon={{
                        name: 'chevron-left',
                        size: 15,
                        color: Colors.defaultColor,
                    }}
                    buttonStyle={{
                        backgroundColor: "transparent",
                    }}
                    color={Colors.defaultColor}
                    title={Language.billing.previous}
                    onPress={this.firstButton.bind(this)}
                />
            )
        }
    }

    showMessage(message) {
        this.setState({
            message: message,
            showAlertMessage: true
        });
    }

    firstButton() {
        var i = this.state.currentPage - 1;

        this.viewPager.setPage(i)

    }

    secondButton() {

        if (this.state.currentPage === 0) {

            this.refs.general.saveLastedClient()


        } else {

            var client = this.getDefaulClient();
            var orderDetail = [];

            var products = realm.objects('OrderDetail');

            for (i = 0; i < products.length; i++) {

                var theProduct = products[i];

                item = {
                    id: theProduct.id,
                    idCategoria: 0,
                    ordenId: 0,
                    cantidad: theProduct.quantity,
                }

                orderDetail.push(item);
            }

            axios(
                {
                    method: 'post',
                    url: url,
                    responseType: 'json',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                },
                {
                    cliente: {
                        id: 0,
                        nombre: client.name,
                        noTelefono: client.phone,
                        correo: client.email,
                        direccion: client.adress
                    },
                    pedido: orderDetail,
                    entidad: Config.entityCode,
                    pedidoGeneral: {
                        mesa: '',
                        tipoOrden: Config.orderType.delivery,
                        codigoPromo: '',
                        valorPromo: 0,
                        pago: Config.paymentMethodCode
                    },
                    noPedido: '0',
                    token: ''

                }
            ).then(response => {

                // Delete all products

                for (i = 0; i < products.length; i++) {
                    realm.write(() => {
                        products[i].delete()
                    })

                }

                this.props.navigation.pop();

            }).catch(function (error) {
                console.log(error)
            });

        }
    }

    navigateToSecondPage() {

        var i = this.state.currentPage + 1;

        this.viewPager.setPage(i)

    }

    getDefaulClient() {
        return realm.objects('Client')[0];
    }
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#ffffff',
            width: '100%',
            height: '100%'
        },
        stepIndicator: {
            paddingTop: 10,
            backgroundColor: '#fff',
            elevation: 5
        },
        navContainer: {
            paddingBottom: 10,
            paddingTop: 10,
            backgroundColor: '#fff',
            elevation: 10,
        },
        page: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }
    })
;