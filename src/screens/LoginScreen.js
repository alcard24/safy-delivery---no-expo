// React imports
import React, {Component} from 'react';
import {
    View,
    Image,
    ImageBackground,
    StyleSheet
} from 'react-native';
import {Text, Button, Icon} from 'react-native-elements';

// Custom library Import
import Input from '../components/common/Input'
import Language from '../constants/Language'
import Error from '../components/common/Error'
import Colors from "../constants/Colors";
// Login Screen Component
export default class LoginScreen extends Component {

    state = {
        email: "",
        password: "",
        hasError: "",
        errorMsg: "",
        loading: false
    }


    /**
     * Render Function
     * @returns {ImageBackground}
     */
    render() {
        return (
            <ImageBackground
                source={require('../assets/images/loginBackGround.png')}
                style={style.background}>
                <View
                    style={style.container}>
                    <Image
                        style={style.logo}
                        source={require('../assets/images/logosafy.png')}/>
                    <Input
                        placeholder={Language.login.user}
                        isEmail={true}
                        icon="user"
                        onSubmit={this.login.bind(this)}
                        onChange={(text) => this.setState({email: text})}
                        style={style.marginBottom}
                    />
                    <Input
                        placeholder={Language.login.password}
                        isPassword={true}
                        icon="lock"
                        onSubmit={this.login.bind(this)}
                        onChange={(text) => this.setState({password: text})}
                    />
                    <Button
                        title={Language.login.loginBtn}
                        raised
                        onPress={this.login.bind(this)}
                        containerViewStyle={style.btnContainer}
                        buttonStyle={style.loginBtn}
                        loading={this.state.loading}
                    />
                    <Button
                        title={Language.login.signupBtn}
                        raised
                        onPress={this.singUp.bind(this)}
                        containerViewStyle={style.btnContainer}
                        buttonStyle={style.signupBtn}/>
                    <Error hasError={this.state.hasError} errorMsg={this.state.errorMsg}/>
                </View>
            </ImageBackground>
        )
    }

    /**
     * Login Function, Attempt Login
     */
    login() {

    }

    /**
     * SingUp Function, Attempt SingUp
     */
    singUp() {
        // this.props.navigation.navigate('Signup', {});
        this.props.navigation.navigate("CategoriesScreen", {
            title: 'HOLA',
            idCategory: 1074
        });
    }

}

const style = StyleSheet.create({
    background: {
        flex: 1
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 12,
        paddingRight: 12
    },
    logo: {
        width: 150,
        height: 150
    },
    title: {
        fontSize: 18,
        color: '#fff',
        fontWeight: 'bold',
        marginBottom: 10
    },
    btnContainer: {
        width: '100%',
        marginTop: 10
    },
    loginBtn: {
        backgroundColor: Colors.colorAccent
    },
    signupBtn: {
        backgroundColor: Colors.primaryColor
    },
    marginBottom: {
        marginBottom: 5
    },
    icon: {
        position: 'absolute',
        top: 15,
        right: 15,
        width: 40,
        height: 40
    }
});