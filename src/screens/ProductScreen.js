// React native and others libraries imports

import React, {Component} from 'react';
import {StyleSheet, View, RefreshControl, ScrollView} from 'react-native';
import axios from 'axios'
import GridList from 'react-native-grid-list'
import AwesomeAlert from 'react-native-awesome-alerts';

import Toast, {DURATION} from 'react-native-easy-toast'

// Our custom files and classes import

import Config from "../constants/Config";
import Product from '../components/Product'

export default class ProductScreen extends Component {

    state = {
        products: [],
        refreshing: false,
    }

    componentDidMount() {
        this.getProducts();
    }

    render() {
        return (
            <View>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>
                    {
                        this.state.products.map((item, index) => {
                            return (
                                <Product
                                    key={index}
                                    item={item}
                                    context={this}
                                />
                            )
                        })
                    }
                </ScrollView>
                <AwesomeAlert
                    showProgress={true}
                    show={this.state.refreshing && Config.showSweetRefreshing}
                />

                <Toast position={'bottom'} ref="toast"/>
            </View>
        );
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        this.getProducts();
    }

    showToast(productName) {
        this.refs.toast.show(productName + ' ' + 'agregado', 500)
    }


    getProducts() {

        var url = Config.endpoints.host + Config.endpoints.productsByCategory + this.props.navigation.state.params.category;

        axios(
            {
                method: 'get',
                url: url,
                responseType: 'json'
            })
            .then(response => {
                const categories = response.data;
                this.setState({
                    products: categories,
                    refreshing: false
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}

const styles = StyleSheet.create({});
