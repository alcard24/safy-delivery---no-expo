// React native and others libraries imports

import React, {Component} from 'react';
import {StyleSheet, View, RefreshControl} from 'react-native';
import axios from 'axios'
import GridList from 'react-native-grid-list'
import AwesomeAlert from 'react-native-awesome-alerts';

// Our custom files and classes import

import Config from "../constants/Config";
import Category from '../components/common/Category'

export default class CategoryScreen extends Component {

    state = {
        categories: [],
        refreshing: false,
    }

    componentDidMount() {
        this.getCategories();
    }

    renderItem = ({item, index}) => (
        <Category
            key={index}
            item={item}
            nav={this.props.navigation}
            screen={true}
        />

    );

    render() {
        return (
            <View>
                <GridList
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    data={this.state.categories}
                    numColumns={2}
                    renderItem={this.renderItem.bind(this)}
                />
                <AwesomeAlert
                    showProgress={true}
                    show={this.state.refreshing && Config.showSweetRefreshing}
                />
            </View>
        );
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        this.getCategories();
    }


    getCategories() {
        var url = Config.endpoints.host + Config.endpoints.childCategories + this.props.navigation.state.params.idCategory;

        axios(
            {
                method: 'get',
                url: url,
                responseType: 'json'
            })
            .then(response => {
                const categories = response.data;
                this.setState({
                    categories: categories,
                    refreshing: false
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}

const styles = StyleSheet.create({});
