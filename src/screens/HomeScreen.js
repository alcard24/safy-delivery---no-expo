import React, {Component} from 'react';
import {
    Image,
    Text,
    ListView,
    View,
    StyleSheet
} from 'react-native';

const TabIcon = ({selected, title, iconName}) => {
    return (
        <View>
            <Icon
                name={iconName}
                color={selected ? '#473332' : '#bdb8bc'}
                size={35}
            />
        </View>
    )
};


export default class HomeScreen extends Component {

    render() {
        return (
            <View
                style={styles.container}

            />
        );
    }

    retrieveModels() {

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    tabBarSelectedItemStyle: {
        borderBottomWidth: 2,
        borderBottomColor: 'red',
    },
});
