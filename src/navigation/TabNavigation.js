// React imports and other libraries
import React, {Component} from 'react'
import {createMaterialTopTabNavigator} from 'react-navigation'
import {Icon} from 'react-native-elements';
import {View} from 'react-native'
// Custom imports
import Colors from '../constants/Colors'


import Items from '../components/tabs/Items'
import Categories from '../components/tabs/Categories'
import Order from '../components/tabs/Orders'

const TabNavigation = createMaterialTopTabNavigator({
        Categories: {
            screen: Categories
        },
        Shop: {
            screen: Items
        },
        Order: {
            screen: Order
        }
    },
    {
        navigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, tintColor}) => {
                const {routeName} = navigation.state;
                let iconName;
                if (routeName === 'Categories') {
                    iconName = 'store';
                } else if (routeName === 'Shop') {
                    iconName = 'shopping-basket';
                } else {
                    iconName = 'receipt';
                }

                return (
                    <View>
                        <Icon
                            name={iconName}
                            type='FontAwesome5'
                            color='#fff'
                        />
                    </View>
                );
            },
        }),
        tabBarOptions: {
            activeTintColor: '#000',
            inactiveTintColor: 'gray',
            style: {
                backgroundColor: Colors.primaryColor,
            },
            indicatorStyle: {
                backgroundColor: '#fff',
            },
            showIcon: true,
            showLabel: false,
            iconColor: '#fff',
        },
    });

export default TabNavigation;