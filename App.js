// React Import
import React, {Component} from 'react';

// Custom Libraries Import
import Main from './src/components/Main';

export default class App extends Component<{}> {
    /**
     * Constructor of App
     * @param props, Properties
     */
    constructor(props) {
        super(props);
        this.state = {realm: null};
    }

    render() {
        return (
            <Main/>
        );
    }
}
